import { useRouter, BlitzPage } from "blitz"
import Layout from "app/core/layouts/Layout"

const FoobarPage: BlitzPage = () => {
  return (
    <div>
      My Foobar
    </div>
  )
}

FoobarPage.redirectAuthenticatedTo = "/"
FoobarPage.getLayout = (page) => <Layout title="FOOOOO">{page}</Layout>

export default FoobarPage
